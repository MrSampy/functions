const getSum = (str1, str2) => {
  if(typeof(str1)!="string" || typeof(str2)!="string")
    return false;
  if((isNaN(str1.length!=0 && parseInt(str1))) || (isNaN(parseInt(str2)) && str2.length!=0))
    return false;
  return (parseInt(str1.length==0?'0':str1)+parseInt(str2.length==0?'0':str2)).toString();
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let postscounter = 0;
  let commentscounter = 0;
  for (const obj of listOfPosts) {
    if(obj.author==authorName)
      ++postscounter;
    if("comments" in obj)
      for (const comment of obj.comments) {
        if(comment.author==authorName)
          ++commentscounter;
      }
  }
  return `Post:${postscounter},comments:${commentscounter}`;
};

const tickets=(people)=> {
  let counter = 0;
  for (const money of people.map(x=>parseInt(x))) {
    counter+=25;
    if(money>25){
      counter-=money-25;  
      if(counter<0)
        return `NO`;
    }
  }
  return `YES`;
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
